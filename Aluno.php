<?php 

include_once './Conta.php';
class Aluno extends Conta{
    
    private $matricula;
    private $curso;
    
    
    public function __construct($nome,$login,$tipoConta,$matricula,$curso){
        parent::__construct($nome, $login, $tipoConta);
        self::setMatricula($matricula);
        self::setCurso($curso);
        parent::setSaldo(25);
    }
    
    
     
    
    
    public function sacar (){
        echo 'Sacar <br>' ;
    }
    
    public function depositar (){
        echo 'Depositar <br>' ;
    }
    
    
    
    /**
     * @return mixed
     */
    public function getMatricula()
    {
        return $this->matricula;
    }

    /**
     * @return mixed
     */
    public function getCurso()
    {
        return $this->curso;
    }

    /**
     * @param mixed $matricula
     */
    public function setMatricula($matricula)
    {
        $this->matricula = $matricula;
    }

    /**
     * @param mixed $curso
     */
    public function setCurso($curso)
    {
        $this->curso = $curso;
    }

  


    
    
    
    
}


?>
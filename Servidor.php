<?php 

include_once './Conta.php';

class Servidor extends Conta{
    
    private $codigo;
    public $cargo;
    
    
    public function __construct($nome,$login,$saldo,$tipoConta,$codigo,$cargo){
        parent::__construct($nome, $login, $saldo, $tipoConta);
        self::setCodigo($codigo);
        self::setCargo($cargo);
    }
    
    
    
    
    
    public function sacar (){
        echo 'Sacar <br>' ;
    }
    
    public function depositar (){
        echo 'Depositar <br>' ;
    }
       
    
    
    
    /**
     * @return mixed
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * @return mixed
     */
    public function getCargo()
    {
        return $this->cargo;
    }

    /**
     * @param mixed $codigo
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    }

    /**
     * @param mixed $cargo
     */
    public function setCargo($cargo)
    {
        $this->cargo = $cargo;
    }

    
    
    
    
    
    
    
}






?>
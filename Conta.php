<?php 

class Conta{
    
    private $nome;
    public $login;
    public $saldo;
    public $tipoConta;
    
    
    public function __construct($nome,$login,$tipoConta){
        self::setNome($nome);
        self::setLogin($login);
        self::setTipoConta($tipoConta);
    }
    
       
    
    
    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @return mixed
     */
    public function getSaldo()
    {
        return $this->saldo;
    }

    /**
     * @return mixed
     */
    public function getTipoConta()
    {
        return $this->tipoConta;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @param mixed $login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @param mixed $saldo
     */
    public function setSaldo($saldo)
    {
        $this->saldo = $saldo;
    }

    /**
     * @param mixed $tipoConta
     */
    public function setTipoConta($tipoConta)
    {
        $this->tipoConta = $tipoConta;
    }

    
    
    
    
    }


?>